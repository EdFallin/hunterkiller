/**/

import ATestSource from "risei/ATestSource";
import Interims from "../system/Interims.js";
import { JSDOM } from "jsdom";

export class InterimsTests extends ATestSource {
    doc;
    dom;
    root;
    h1Node;
    divNode;
    subNode;
    leaf;

    supplyDom() /* verified */ {
        try {
            let imitation = new JSDOM();
            this.doc = imitation.window.document;
            let doc = this.doc;

            /* Mini-DOM with two branches. */

            this.root = doc.createElement("BODY");
            this.subNode = doc.createElement("DIV");
            this.leaf = doc.createElement("P");
            this.h1Node = doc.createElement("H1");
            this.divNode = doc.createElement("DIV");

            // Short branch.
            this.root.appendChild(this.h1Node);
            
            // Long branch.
            this.root.appendChild(this.divNode);
            this.divNode.appendChild(this.subNode);
            this.subNode.appendChild(this.leaf);
        }
        catch (thrown) {
            console.log(`cruft : thrown:`, thrown);
        }
    }
    
    /* DOM and its elements aren't available for .in at 
       time tests are read, so this is needed in .do. */
    setInputNodeText(test, node, text) /* verified */ {
        test.in[0] = node;  // Not available at read time.
        node.innerText = "target";
    }

    domDoesContainNode(node) /* verified */ {
        let root = this.root;
        let doesContain = root.contains(node);
        return doesContain;
    }

    tests = [
        { on: Interims, with: [ ] },

        { of: "killNodeWithSubtreeText" },
        { for: "When a node's own text is the targeted text, the node is removed.", /* good */
          in: [ "TBR", "target" ], out: false,  // false = no longer in DOM.
          do: { 
            late: (test) => { 
                    this.supplyDom();
                    test.in[0] = this.h1Node;  // Not available at test-read time.
                    this.h1Node.innerText = "target";
              } 
            },
          from: (test, actual) => { return this.domDoesContainNode(this.h1Node); } 
        },
        { for: "When a node contains a descendant with the targeted text, the node is removed.", /* good */
          in: [ "TBR", "target" ], out: false,  // false = no longer in DOM.
          do: { 
            late: (test) => { 
                    this.supplyDom();
                    test.in[0] = this.divNode;  // Not available at test-read time.
                    this.leaf.innerText = "target";
              } 
            },
          from: (test, actual) => { return this.domDoesContainNode(this.divNode); } 
        },
        { for: "When neither a node nor its descendants have have the targeted text, nothing is changed.", /* good */
          in: [ "TBR", "target" ], out: true,  // true = still in DOM.
          do: { 
            late: (test) => { 
                    this.supplyDom();
                    test.in[0] = this.divNode;  // Not available at test-read time.
                    this.leaf.innerText = "something else";
              } 
            },
          from: (test, actual) => { return this.domDoesContainNode(this.divNode); } 
        },
    ];
}
