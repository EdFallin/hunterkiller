
import ATestSource from "risei/ATestSource";
import { Recaster } from "../system/Recaster.js";
import { RecastTypes } from "../system/RecastTypes.js";

export class RecasterTests extends ATestSource {
    tests = [
        { on: Recaster, with: [ ] },

        { of: "replaceAll" },
        { from: (test, actual) => { return test.in[0]; } },

        { for: "Replaces all plain-text matches in text.", /* good */
          in: [ { nodeValue: "A word and a word and a word." }, "word", "lexeme" ], 
          out: { nodeValue: "A lexeme and a lexeme and a lexeme." } },
        { for: "Replaces all regex matches in text when regex uses /g.", /* good */
          in: [ { nodeValue: "A word and a worn and a worm." }, /wor[dnm]/g, "thing" ], 
          out: { nodeValue: "A thing and a thing and a thing." } },
        { for: "Replaces all regex matches in text when regex uses `g` flag.", /* good */
          in: [ { nodeValue: "A 123 and a 456 and a 789." }, new RegExp("\\d\\d\\d", "g"), "number" ], 
          out: { nodeValue: "A number and a number and a number." } },


        { of: "chooseOpBy" },
        { plus: [ ] },

        { for: "Returns Recaster's recastTargetAsIs() for RecastTypes.AsIs.", /* good */
          in: [ RecastTypes.AsIs ],
          out: Recaster.prototype.recastTargetAsIs },
        { for: "Returns Recaster's recastWholeWord() for RecastTypes.WholeWord.", /* good */
          in: [ RecastTypes.WholeWord ],
          out: Recaster.prototype.recastWholeWord },
        { for: "Returns Recaster's recastCaseMatch() for RecastTypes.CaseMatch.", /* good */
          in: [ RecastTypes.CaseMatch ],
          out: Recaster.prototype.recastCaseMatch },
        { for: "Returns Recaster's recastInflections() for RecastTypes.Inflections.", /* good */
          in: [ RecastTypes.Inflections ],
          out: Recaster.prototype.recastInflections },


        { of: "recastTargetAsIs" },
        { plus: [ ] },
        { from: (test, actual) => { return test.in[0]; } },

        { for: "Replaces all plain-text matches in text.", /* good */
          in: [ 
                { nodeValue: "A word and a word and a word." }, 
                { from: "word", to: "lexeme", type: 0 } 
          ], 
          out: { nodeValue: "A lexeme and a lexeme and a lexeme." } },
        { for: "Replaces all regex matches in text when regex uses /g.", /* good */
          in: [ 
                { nodeValue: "A word and a worn and a worm." }, 
                { from: /wor[dnm]/g, to: "thing", type: 0 } 
          ], 
          out: { nodeValue: "A thing and a thing and a thing." } },
        { for: "Replaces all regex matches in text when regex uses `g` flag.", /* good */
          in: [ 
                { nodeValue: "A 123 and a 456 and a 789." }, 
                { from: new RegExp("\\d\\d\\d", "g"), to: "number", type: 0 } 
          ], 
          out: { nodeValue: "A number and a number and a number." } },
    
    ];
}
