
import ATestSource from "risei/ATestSource";
import Expression from "../system/Expression.js";

export class ExpressionTests extends ATestSource {
/* Tests of individual expressions using a stripped-down 
   replacer class as a convenience.  See target class. */

    static polyText;

    tests = [
        { on: Expression, with: [ ] },
        { of: "replace" },

        /* Apostrophe-s. */
        { for: "s's to s'", /* good */
          in: [ "cats's claws", { from: /s's/g, to: "s'" } ], 
          out: "cats' claws" },  
        { for: "s\u2019s to s\u2019", /* good */
          in: [ "cats\u2019s claws", { from: /s('|\u2019|’)s/g, to: "s$1" } ], 
          out: "cats\u2019 claws" },
        { for: "s’s to s’", /* good */
          in: [ "cats’s claws", { from: /s('|\u2019|’)s/g, to: "s$1" } ],
          out: "cats’ claws" },

        /* Enroll with only two l's. */
        { for: "enroll alone, from one to two l's", /* good */
          in: [ "enrol in the class", { from: /([Ee])nrol(?!l)/g, to: "$1nroll" } ],
          out: "enroll in the class" },
        { for: "enroll alone, capitalized, from one to two l's", /* good */
          in: [ "Enrol in the class", { from: /([Ee])nrol(?!l)/g, to: "$1nroll" } ],
          out: "Enroll in the class" },
        { for: "enroll mid-word, different casings, from one to two l's", /* good */
          in: [ 
            "The enrolment was low because few people were enroling.  Enrolment was low.", 
            { from: /([Ee])nrol(?!l)/g, to: "$1nroll" } 
          ],
          out: "The enrollment was low because few people were enrolling.  Enrollment was low." },
        { for: "enroll starting with two l's, unchanged", /* good */
          in: [ 
            "Enroll in the class because enrolling increases enrollment after you enroll.", 
            { from: /([Ee])nrol(?!l)/g, to: "$1nroll" } 
          ],
          out: "Enroll in the class because enrolling increases enrollment after you enroll." },

    ];
}
