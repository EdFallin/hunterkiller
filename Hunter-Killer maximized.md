
# Hunter-Killer Maximized

Here are things I can put under test in Hunter-Killer, plus future things I can do, possibly soon.&nbsp;  Some or all of the futurings may be done as stages.

I would do each futuring under test.

> The first futuring, then, ahead of the listed ones, would be to retrofit things as a NodeJs app, for testing purposes only using Rjs, and hopefully avoiding most of the default stuff.&nbsp;  Probably I can just add a `package.json` and do an `npm install` that includes my package.

## Testable

A series of words, all in one text, are changed only when solo words.

Problem terms that are OK in some circumstances and not in others.

Something about acronyms if possible.



## Futurings and stages

Certain sites skip scans at all.

List/s of removals and replaces in a simple UI, still with regex possible.

Easy whole-word option instead of hand-crafted regex in UI.

Matching capitalization in code.

Easy replace-matching-capitalization option in UI.

Organization into graphs that are notionally or physically bifurcated (either two root lists with two kinds of nodes that cross-reference each other, or one root list with two notional segments, and two kinds of nodes).
- The entry point is the page's URL root, which may get further matched if adjacent graph nodes have URL paths, etc.
- Once a match is found (no further matches are adjacent), then matching remove / rewrite nodes are found and their removals / rewrites performed.

UI that constructs graph somehow or other, possibly using dnd right away.

If UI was built first without dnd, then dnd or something similar in the UI.

Click-to-re-kill for a page, that runs the process again, for instance after some late-breaking B.S. like that seen at YT.

Click-to-kill / click-to-fix for anything, either from a R-click or a button followed by a selecting click, that brings up a dialog to allow selecting whether to remove or rewrite, with what scope (page, etc.), and so on.

Also a kill-now option on R-click or after a button-click, that just destroys / removes the selected element without any other options.

Also maybe a rewrite-now option from similar interactions, to replace a certain word everywhere just on the page, with easy whole-word and capitalization options available.

