/**/

import { RecastTypes } from "./RecastTypes.js";

export class Recaster {
    #opsByType = new Map([
        [ RecastTypes.AsIs, this.recastTargetAsIs ],
        [ RecastTypes.WholeWord, this.recastWholeWord ],
        [ RecastTypes.CaseMatch, this.recastCaseMatch ],
        [ RecastTypes.Inflections, this.recastInflections ],
    ]);

    recastAll(textNode, targets) {
        for (let target of targets) {
            // Operations vary, but rely on other members.
            let recastOp = this.chooseOpBy(target.type);
            recastOp.bind(this);
            
            // Replacing simply or complexily 
            // as defined by the target.type.
            recastOp(textNode, target);
        }
    }
    
    chooseOpBy(type) /* passed */ {
        let recastOp = this.#opsByType.get(type);
        return recastOp;
    }
    
    recastTargetAsIs(textNode, target) /* passed */ {
        /* As-is recasting just uses whatever was provided. */
        this.replaceAll(textNode, target.from, target.to);
    }
    
    recastWholeWord(textNode, target) { 
        /* Whole-word uses regex to match only whole words, 
           with "g" / global flag to replace all matches. */

        // plate -> dish, platelet -> platelet

        let fromText = `\b${ target.from }\b`;
        let from = new RegExp(fromText, "g");
        
        this.replaceAll(textNode, from, target.to);
    }
    
    recastCaseMatch(textNode, target) {
        /* Case-match finds text as whole words in either 
           initial casing, and matches casing in output. */

        // Magma -> Lava, magma -> lava, Magmatic -> Magmatic

        let lowerFrom = target.from;
        lowerFrom[0] = lowerFrom[0].toLowerCase();
        
        let lowerTo = target.to;
        lowerTo[0] = lowerTo[0].toLowerCase();
        
        let upperFrom = target.from;
        upperFrom[0] = upperFrom[0].toUpperCase();
        
        let upperTo = target.to;
        upperTo[0] = upperTo[0].toUpperCase();

    }
    
    recastInflections(textNode, target) {
        
    }

    replaceAll(textNode, from, to) /* passed */ {
        // Actually replacing text for any recasting.
        let text = textNode.nodeValue.replaceAll(from, to);
        textNode.nodeValue = text;
    }

}

