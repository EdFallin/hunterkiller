/**/

export class RecastTypes {
    static AsIs = 0;
    static WholeWord = 1;
    static CaseMatch = 2;
    static Inflections = 3;
}
