/**/

export default class Interims {
    /* This class is a place where I can try out things 
       and get them working under test, separately from 
       my long-term intended overhaul of H-K. */

    killNodeWithSubtreeText(node, text) /* passed */ {
        // Text may be anywhere in subtree.
        let isPresent = this.#recurseTreeToFindText(node, text);

        // Actually killing node.
        if (isPresent) {
            let parent = node.parentNode;
            parent.removeChild(node);
        }
    }
    
    #recurseTreeToFindText(node, text) /* verified */ {
        // This level.
        if (this.#hasText(node, text)) {
            return true;
        }

        // Next level: recursion.
        for (let child of node.childNodes) {
            return this.#recurseTreeToFindText(child, text);
        }
    }
    
    #hasText(node, text) /* verified */ {
        if (node.innerText === undefined) {
            return false;
        }
        
        return node.innerText.includes(text);
    }

}
