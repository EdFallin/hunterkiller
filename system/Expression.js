
/* This class provides a succinct way to test individual recasting expressions without 
   Recaster (which is changed up) or HunterKiller (which is wired to a browser context). */

export default class Expression {

    /* Works the same as replaceText() in HunterKiller except 
       on arg text, rather than an arg text node's .nodeValue. */
    replace(text, target) {
        let from = target.from;
        let to = target.to;

        text = text.replaceAll(from, to);

        return text;
    }
}