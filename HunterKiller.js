/**/

// region Targets

let killables = [
    "#most-viewed-right",
    "#most-viewed-footer",
    "img[alt*=\"trump\" i]",
    "a[href*=\"trump\" i] img",
    "script[src*=\"newsletter\" i]",
    "div[class*=\"ad-wrapper\"]",
    "[class*=\"ncm-\"]",
    "#ensNotifyBanner",
    "iron-selector",
    "newsletter-campaign",
    "ps-newsletter-module",
    "script#head-ncm",
    "[data-id*=\"trump\" i] img",
    "metering-bottompanel",
    ".c-overlay-fullscreen",
    ".byline-data",
    ".site-message--banner",
    "#bottom-banner",
    "[srcset*=\"Mahdawi\"]",
    "[src*=\"Mahdawi\"]",
    "#slot-body-end",
    "[data-gu-name=\"right-column\"]",
    "#thrasher__documentary-template-2022",
    ".cookies-popup",
    ".jetbrains-cookies-banner",
    "[data-spacefinder-type*=\"Newsletter\"]",
    "#browserUpgradeNoticeBar",
    "#m-gnav-header",
    "#thrasher__first-thing-thrasher",
    "ins",
    "gu-island",
    "[id^=\"portalBanner\"]",
    "[id^=\"siteNotice\"]",
    "[id^=\"centralNotice\"]",
    ".banner",
    "#credentials-picker-container",
    "#animated-container",
    "[src=\"https://accounts.google.com\"]",
    "[title=\"Sign in with Google Dialog\"]",
    ".flex-popup",
    ".flex-popup--visible",
    ".notifications-prompt-container",
    ".css-zdpfni",
    "tp-yt-paper-dialog",
    ".cookie-bar",
    ".cookie-notification",
    "section[class^=\"trump-on-trial\"]",
    "[href*=\"trump\" i] picture",
    "[href*=\"trump\" i] ~ div picture",
    "[class*=\"us-primaries-thrasher\"]",
    "[id^=\"thrasher__multi-newsletter\"]",
    "ytd-display-ad-renderer",

];

let recastables = [
    { from: /[Mm]ale [Gg]aze/g, to: "xxx xxx" },
    { from: /\bqueer\b/g, to: "gay" },
    { from: /\bQueer\b/g, to: "Gay" },
    { from: "Latinx", to: "Latine" },
    { from: /\b([Ss])hit\b/g, to: "$1---" },
    { from: /\b([Ss])hitty\b/g, to: "$1---ty" },
    { from: /\b([Ss])hitting\b/g, to: "$1---ting" },
    { from: /\b([Ss])hittiness\b/g, to: "$1---tiness" },
    { from: /\b([Ss])hitstorm\b/g, to: "$1---storm" },
    { from: /\b([Ss])hitshow\b/g, to: "$1---show" },
    { from: /\b([Ss])hithead\b/g, to: "$1---head" },
    { from: /\ben([Ss])hittification\b/g, to: "en$1---tification" },
    { from: /([Ff])uck/g, to: "$1---" },
    { from: /([Gg])lobal heating/g, to: "$1lobal warming" },
    { from: "The Week in Patriarchy", to: "----" },
    { from: /\b([Mm])aths/g, to: "$1ath" },
    { from: /\b([Aa])rtefact/g, to: "$1rtifact" },
    { from: /\b([Ff])urore/g, to: "$1uror" },
    { from: /\d+ min\w*? to read/g, to: "" },
    { from: /\d+ min\w*? read/g, to: "" },
    { from: /\d+ contributors/g, to: "" },
    { from: "cisgender", to: "hetero" },
    { from: "Cisgender", to: "Hetero" },
    { from: "jumper", to: "sweater" },
    { from: "Jumper", to: "Sweater" },
    { from: /([Aa])geing/g, to: "$1ging" },
    { from: /([Aa])luminium/g, to: "$1luminum" },
    { from: "Arwa", to: "-- Do not " },
    { from: "Mahdawi", to: "read this --" },
    { from: /\bnerd/g, to: "expert" },
    { from: /\bNerd/g, to: "Expert" },
    { from: /\bNERD/g, to: "EXPERT" },
    { from: /([Ee])nrol(?!l)/g, to: "$1nroll" },
    { from: /([Ff])ulfil(?!l)/g, to: "$1ulfill" },
    { from: "in hospital", to: "in the hospital" },
    { from: /[Tt]he \w+ in [Pp]atriarchy/g, to: "----" },
    { from: "Rebecca Solnitt", to: "-- Do not read this --" },
    { from: "Marina Hyde", to: "-- Do not read this --" },
    { from: "Gene Marks", to: "-- Do not read this --" },
    { from: /\bie\b/g, to: "i.e." },
    { from: /\b([Aa])s per\b/g, to: "$1ccording to" },
    { from: /\bper (?=(the|a|an|its|our|research)\b|[A-Z])/g, to: "according to " },
    { from: /\bPer (?=(the|a|an|its|our|research)\b|[A-Z])/g, to: "According to " },
    { from: "Per usual", to: "As usually seen" },
    { from: "per usual", to: "as usually seen" },
    { from: /\b([Dd]ifferent) to\b/g, to: "$1 than" },
    { from: "is a creature who writes", to: "exists" },
    { from: /\b(a|the) disconnect\b/g, to: "$1 separation" },
    { from: /s('|\u2019|’)s/g, to: "s$1" },
    { from: /(st|nd|rd|th) series/g, to: "$1 season" },
    { from: /\b([Tt])itbit(s)?\b/g, to: "$1idbit$2" },
    { from: "Gobsmack", to: "Dumbfound" },
    { from: "gobsmack", to: "dumbfound" },
    { from: /([Gg])awp/g, to: "$1awk" },

    /* Acronyms. */
    { from: /\bNato\b/g, to: "NATO" },
    { from: /\bFifa\b/g, to: "FIFA" },
    { from: /\bNasa\b/g, to: "NASA" },
    { from: /\bOpec\b/g, to: "OPEC" },
    { from: /\bDaca\b/g, to: "DACA" },
    { from: /\bAids\b/g, to: "AIDS" },
    { from: /\bPac\b/g, to: "PAC" },
    { from: /\b[Aa]wol\b/g, to: "AWOL" },
    { from: /\bNorad\b/g, to: "NORAD" },
    { from: /\bPacs\b/g, to: "PACS" },
    { from: /\bMaga\b/g, to: "MAGA" },
    { from: /\bBafta\b/g, to: "BAFTA" },
    { from: /\bDarpa\b/g, to: "DARPA" },
    { from: /\bSag-Aftra\b/g, to: "SAG-AFTRA" },
    { from: /\bFema\b/g, to: "FEMA" },
    { from: /\bFomo\b/g, to: "FOMO" },
    
];

// endregion Targets

// region Crux class

// Contains all operations to be run.
class HunterKiller {
    #id = "HunterKillerDidRunBox";

    constructor() {
        /* No operations. */
    }

    // region Hunting-killing

    huntKill(targets) {
        let target = targets.join(", ");

        let dom = document;
        let nodes = dom.querySelectorAll(target);

        for (let node of nodes) {
            let parent = node.parentNode;
            parent.removeChild(node);
        }
    }

    // endregion Hunting-killing

    // region Recasting

    recast(targets) {
        let dom = document;
        let root = document.body;

        // DOM querying doesn't work for text contents.
        this.recastRecurse(root, targets);
    }

    recastRecurse(node, targets) {
        // Scripts are not changed, for now at least.
        if (node.tagName === "SCRIPT") {
            return;
        }

        // This level.  Recasting child text leaves.
        for (let child of node.childNodes) {
            if (child.nodeType === Node.TEXT_NODE) {
                this.replaceText(child, targets);
            }
        }

        // Next level: Actual recursion over non-leaf nodes.
        for (let child of node.childNodes) {
            if (child.nodeType !== Node.TEXT_NODE) {
                this.recastRecurse(child, targets);
            }
        }
    }

    replaceText(textNode, targets) {
        for (let target of targets) {
            let from = target.from;
            let to = target.to;

            // Crux: Actually recasting.
            let text = textNode.nodeValue.replaceAll(from, to);
            textNode.nodeValue = text;
        }
    }

    // endregion Recasting

    // region Run stripe

    addDidRunStripe() {
        let box = document.createElement("DIV");
        box.id = this.#id;

        box.style.fontFamily = "sans-serif";
        box.style.fontSize = "125%";
        box.style.textAlign = "center";
        box.style.color = "white";
        box.style.whiteSpace = "pre";

        box.style.border = "1px solid goldenrod";
        box.style.background = "goldenrod";
        box.style.padding = "2px";

        let beforeThis = document.body.firstElementChild;
        document.body.insertBefore(box, beforeThis);
    }

    waitThenSubtractDidRunStripe() {
        let box = document.getElementById(this.#id);
        setTimeout(this.#subtractDidRunStripe.bind(this), 3000);
    }

    #subtractDidRunStripe() {
        let box = document.getElementById(this.#id);
        box.remove();
    }

    // endregion Run stripe
}

// endregion Crux class

// region Run script

// Creating an operation container.
let hk = new HunterKiller();

// Running the operations immediately.
hk.huntKill(killables);
hk.recast(recastables);

hk.addDidRunStripe();
hk.waitThenSubtractDidRunStripe();

// And running them again later for slow-opening pages.
// Workaround: Load events can't be usefully observed.
// Run-at moment is just before subtractor's run-at.
setTimeout(() => {
        hk.huntKill(killables);
        hk.recast(recastables);
    },
    2500
);

// endregion Run script

